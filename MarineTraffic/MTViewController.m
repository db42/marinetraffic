//
//  MTViewController.m
//  MarineTraffic
//
//  Created by Dushyant Bansal on 4/7/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import "MTViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "MTTrafficFetcher.h"
#import "MTShip.h"
#import "MTSearchResultsViewController.h"

static const CLLocationDegrees HongKongLatitude = 22.2855200;
static const CLLocationDegrees HongKongLongitude = 114.1576900;
static const float DefaultZoom = 9;


@interface MTViewController () <GMSMapViewDelegate, UITextFieldDelegate, MTSearchResultsViewControllerDelegate>

@property (nonatomic) MTTrafficFetcher *trafficFetcher;
@property (weak, nonatomic) GMSMapView *mapview;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UIView *searchResultsVCContainer;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIView *topPanel;

@end

@implementation MTViewController

- (MTTrafficFetcher *)trafficFetcher {
  if (_trafficFetcher == nil) {
    _trafficFetcher = [[MTTrafficFetcher alloc] init];
  }
  return _trafficFetcher;
}

- (void)loadMapView {
  GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:HongKongLatitude
                                                          longitude:HongKongLongitude
                                                               zoom:DefaultZoom];
  
  GMSMapView *mapView = [GMSMapView mapWithFrame:self.view.bounds camera:camera];
  mapView.delegate = self;
  [self.view insertSubview:mapView atIndex:0];
  self.mapview = mapView;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.searchTextField.delegate = self;
  self.searchTextField.returnKeyType = UIReturnKeySearch;
  
  [self loadMapView];
  [self registerForKeyboardNotifications];
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  [self fetchShipsLocationsAndDrawMarkers];
}

- (void)viewDidLayoutSubviews {
  [super viewDidLayoutSubviews];
  [self updateMapViewFrame];
}

- (void)updateMapViewFrame {
  CGRect frame = self.mapview.frame;
  frame.size.width = self.view.bounds.size.width;
  frame.size.height = self.view.bounds.size.height;
  
  self.mapview.frame = frame;
}

- (void)fetchShipsLocationsAndDrawMarkers {
  GMSMapView *mapView = (GMSMapView *)self.mapview;
  GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithRegion:mapView.projection.visibleRegion];
  float sw_x = bounds.southWest.longitude;
  float sw_y = bounds.southWest.latitude;
  
  float ne_x = bounds.northEast.longitude;
  float ne_y = bounds.northEast.latitude;
  float zoom = mapView.camera.zoom;
  
  [self.trafficFetcher fetchShipsWithInCoordinates:sw_x
                                              sw_y:sw_y
                                              ne_x:ne_x
                                              ne_y:ne_y
                                              zoom:zoom
                                           success:^(NSArray *responseData) {
                                             [self addMarkersToShipsData:responseData];
                                           } failure:nil];
}

- (void)addMarkersToShipsData:(NSArray *)shipsData {
  for (NSArray *shipData in shipsData) {
    MTShip *ship = [MTShip shipFromData:shipData];
    if (ship != nil) {
      [self addMarkerForShip:ship];
    }
  }
}

- (void)addMarkerForShip:(MTShip *)ship {
  CLLocationCoordinate2D location = CLLocationCoordinate2DMake(ship.latitude, ship.longitude);
  GMSMarker *marker = [GMSMarker markerWithPosition:location];
  marker.icon = [UIImage imageNamed:@"marker"]; //glow-marker can also be used;
  marker.map = (GMSMapView *)self.mapview;
  marker.title = ship.name;
}

- (void)clearMarkers {
  [(GMSMapView *)self.mapview clear];
}

- (void)handleShipsData:(id)responseData forSearchResultViewController:(MTSearchResultsViewController *)searchResultViewController {
  //MarineTraffic api is inconsistent.
  //`responseData` can be dictionary as well as array.
  if ([responseData isKindOfClass:[NSDictionary class]]) {
    searchResultViewController.shipsData = [responseData performSelector:@selector(allValues)];
  } else {
    searchResultViewController.shipsData = responseData;
  }
}

- (void)loadSearchResultsForQueryString:(NSString *)searchText {
  MTSearchResultsViewController *searchResultViewController = (MTSearchResultsViewController *)[self.childViewControllers firstObject];
  searchResultViewController.delegate = self;
  
  [self.trafficFetcher searchForVesselOrPortWithText:searchText success:^(id responseData) {
    [self handleShipsData:responseData forSearchResultViewController:searchResultViewController];

    self.searchResultsVCContainer.hidden = NO;
    self.searchResultsVCContainer.userInteractionEnabled = YES;
  } failure:nil];
}

- (IBAction)didPressCancel:(id)sender {
  self.searchTextField.text = @"";
  [self.searchTextField resignFirstResponder];
  
  [self hideAllViewsExceptMapAndSearchTextField];
}

- (void)hideAllViewsExceptMapAndSearchTextField {
  self.searchResultsVCContainer.hidden = YES;
  self.searchResultsVCContainer.userInteractionEnabled = NO;
  
  self.topPanel.hidden = YES;
  self.cancelButton.hidden = YES;
  self.cancelButton.userInteractionEnabled = NO;
}

#pragma mark - GMSMapViewDelegate

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position {
  [self clearMarkers];
  [self fetchShipsLocationsAndDrawMarkers];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
  //  [textField resignFirstResponder];
  
  [self loadSearchResultsForQueryString:textField.text];
  return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
  self.topPanel.hidden = NO;
  self.cancelButton.hidden = NO;
  self.cancelButton.userInteractionEnabled = YES;
}

#pragma mark - MTSearchResultsControllerDelegate

- (void)searchResultsController:(MTSearchResultsViewController *)searchResultsController userDidSelectRowWithIndexPath:(NSIndexPath *)indexPath {
  [self didPressCancel:nil];
  
  //Currently, this does nothing when a search result is selected
  //it can be used to show ship from indexPath on the map
}

#pragma mark - handle keyboard

- (void)registerForKeyboardNotifications
{
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(keyboardWasShown:)
                                               name:UIKeyboardDidShowNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification {
  NSDictionary* info = [aNotification userInfo];
  CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
  
  CGRect frame = self.searchResultsVCContainer.frame;
  
  BOOL isPortrait = UIDeviceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation);
  CGFloat kbHeight = isPortrait ? kbSize.height : kbSize.width;
  CGFloat viewHeight = isPortrait ? self.view.bounds.size.height : self.view.bounds.size.width;
  
  CGFloat height = viewHeight - kbHeight - frame.origin.y;
  frame.size.height = height;
  self.searchResultsVCContainer.frame = frame;
}
@end
