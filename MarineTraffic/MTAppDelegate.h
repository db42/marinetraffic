//
//  MTAppDelegate.h
//  MarineTraffic
//
//  Created by Dushyant Bansal on 4/7/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
