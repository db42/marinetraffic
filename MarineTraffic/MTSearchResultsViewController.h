//
//  MTSearchResultsViewController.h
//  MarineTraffic
//
//  Created by Dushyant Bansal on 4/9/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MTSearchResultsViewControllerDelegate;

@interface MTSearchResultsViewController : UITableViewController
@property (nonatomic) NSArray *shipsData;
@property (nonatomic, weak) id<MTSearchResultsViewControllerDelegate> delegate;

@end

@protocol MTSearchResultsViewControllerDelegate
- (void)searchResultsController:(MTSearchResultsViewController *)searchResultsController userDidSelectRowWithIndexPath:(NSIndexPath *)indexPath;
@end