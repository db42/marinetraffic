//
//  MTTrafficFetcher.m
//  MarineTraffic
//
//  Created by Dushyant Bansal on 4/8/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import "MTTrafficFetcher.h"
#import <AFNetworking/AFHTTPRequestOperation.h>
#import <AFNetworking/AFHTTPRequestOperationManager.h>

static NSString *const BaseShipsLocationURL = @"http://www.marinetraffic.com/ais/getjson.aspx";
static NSString *const BaseShipsSearchURL = @"http://www.marinetraffic.com/fr/map/searchjson";

@implementation MTTrafficFetcher

- (void)fetchShipsWithInCoordinates:(float)sw_x
                               sw_y:(float)sw_y
                               ne_x:(float)ne_x
                               ne_y:(float)ne_y
                               zoom:(float)zoom
                            success:(void (^)(NSArray *))success
                            failure:(void (^)())failure {
  NSDictionary *params = @{@"sw_x": [[NSNumber numberWithFloat:sw_x] stringValue],
                           @"sw_y": [[NSNumber numberWithFloat:sw_y] stringValue],
                           @"ne_x": [[NSNumber numberWithFloat:ne_x] stringValue],
                           @"ne_y":[[NSNumber numberWithFloat:ne_y] stringValue],
                           @"zoom":[[NSNumber numberWithFloat:zoom] stringValue],
                           };
  AFHTTPRequestOperationManager *manager = [self managerAfterSettingHTTPHeader];
 
  [manager GET:BaseShipsLocationURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
    if (operation.responseData && operation.response.statusCode == 200)
    {
      NSError *err;
      NSArray *shipsData = [NSJSONSerialization JSONObjectWithData:operation.responseData
                                                          options:NSJSONReadingAllowFragments
                                                            error:&err];
      success(shipsData);
    }
  } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    NSString *response = [[NSString alloc] initWithData:operation.responseData
                                               encoding:NSASCIIStringEncoding];
    NSLog(@"Error fetching locations results from MarineTraffic - %@", response);
    failure();
  }];
}

- (void)searchForVesselOrPortWithText:(NSString *)searchText success:(void (^)(id))success failure:(void (^)())failure {
  NSDictionary *params = @{@"term": searchText};
  AFHTTPRequestOperationManager *manager = [self managerAfterSettingHTTPHeader];

  [manager GET:BaseShipsSearchURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
    if (operation.responseData && operation.response.statusCode == 200)
    {
      NSError *err;
      id shipsData = [NSJSONSerialization JSONObjectWithData:operation.responseData
                                                          options:NSJSONReadingAllowFragments
                                                            error:&err];
      success(shipsData);
    }
  } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    NSString *response = [[NSString alloc] initWithData:operation.responseData
                                               encoding:NSASCIIStringEncoding];
    NSLog(@"Error fetching search results from MarineTraffic - %@", response);
    failure();
  }];
}

- (AFHTTPRequestOperationManager *)managerAfterSettingHTTPHeader {
  AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
  
  [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
  [manager.requestSerializer setValue:@"http://www.marinetraffic.com/ais/" forHTTPHeaderField:@"Referer"];
  return manager;
}


@end
