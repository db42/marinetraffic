//
//  MTShip.h
//  MarineTraffic
//
//  Created by Dushyant Bansal on 4/9/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTShip : NSObject

@property (nonatomic) NSString *name;
@property (nonatomic) float latitude;
@property (nonatomic) float longitude;
@property (nonatomic) float speed;
@property (nonatomic) float course;

+ (instancetype)shipFromData:(NSArray *)data;
@end
