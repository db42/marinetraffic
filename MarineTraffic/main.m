//
//  main.m
//  MarineTraffic
//
//  Created by Dushyant Bansal on 4/7/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MTAppDelegate.h"

int main(int argc, char * argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([MTAppDelegate class]));
  }
}
