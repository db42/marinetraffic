//
//  MTTrafficFetcher.h
//  MarineTraffic
//
//  Created by Dushyant Bansal on 4/8/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface MTTrafficFetcher : NSObject

- (void)fetchShipsWithInCoordinates:(float)sw_x
                               sw_y:(float)sw_y
                               ne_x:(float)ne_x
                               ne_y:(float)ne_y
                               zoom:(float)zoom
                            success:(void(^)(NSArray *responseData))success
                            failure:(void(^)())failure;

/**
 Calls success block with array/dictionary of ships/vessels matching `searchText`
 MarineTraffic api is inconsistent. `responseData` can be dictionary as well as array.
 
 @param searchText Can be any keyword including MMSI and name
 */
- (void)searchForVesselOrPortWithText:(NSString *)searchText
                              success:(void(^)(id responseData))success
                              failure:(void(^)())failure;
@end
