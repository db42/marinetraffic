//
//  MTShip.m
//  MarineTraffic
//
//  Created by Dushyant Bansal on 4/9/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import "MTShip.h"

@implementation MTShip

+ (instancetype)shipFromData:(NSArray *)data {
  if (![data isKindOfClass:[NSArray class]]) {
    return nil;
  }

  MTShip *ship = [[MTShip alloc] init];
  ship.name = data[2];
  ship.latitude = [(NSString *)data[0] floatValue];
  ship.longitude = [(NSString *)data[1] floatValue];
  return ship;
}

@end
