//
//  MTSearchResultsViewController.m
//  MarineTraffic
//
//  Created by Dushyant Bansal on 4/9/14.
//  Copyright (c) 2014 Dushyant Bansal. All rights reserved.
//

#import "MTSearchResultsViewController.h"

@interface MTSearchResultsViewController ()

@end

@implementation MTSearchResultsViewController

- (void)setShipsData:(NSArray *)shipsData {
  _shipsData = shipsData;
  [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return [self.shipsData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  static NSString *CellIdentifier = @"shipCell";
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
  
  NSDictionary *shipData = self.shipsData[indexPath.row];
  [self configureCell:cell withShipData:shipData];
  return cell;
}

- (void)configureCell:(UITableViewCell *)cell withShipData:(NSDictionary *)shipData {
  cell.textLabel.text = shipData[@"desc"];
  cell.detailTextLabel.text = shipData[@"value"];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [self.delegate searchResultsController:self userDidSelectRowWithIndexPath:indexPath];
}

@end
